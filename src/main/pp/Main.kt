package main.pp

import main.oop.structure.base.Marker
import main.oop.utils.formatFileTime
import main.oop.utils.toIntUnsigned
import java.nio.ByteBuffer
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.attribute.BasicFileAttributes
import java.text.SimpleDateFormat
import java.util.*

const val defaultFileName = "google_favicon.jpg"
val properties = mutableListOf<String>()

fun main(args: Array<String>) {

    val useDefaultFilename = inputUseDefaultFilename()
    val fileName = inputFilename(useDefaultFilename)
    if (fileName.toLowerCase() == "/exit")
        return
    println()
    parseImportant(fileName)
    for (property in properties) {
        println(property)
    }
}

fun inputUseDefaultFilename(): Boolean {
    var result: Boolean
    while (true) {
        try {
            print("Use default picture? (Y̲/N): ")
            result = when (Scanner(System.`in`).nextLine().toLowerCase()) {
                "n" -> false
                "y" -> true
                "" -> true
                else -> throw Exception()
            }
            break
        } catch (e: Exception) {
        }
    }
    return result
}

fun inputPrintOnlyBaseInfo(): Boolean {
    var result: Boolean
    while (true) {
        try {
            print("Print only base info? (Y̲/N): ")
            result = when (Scanner(System.`in`).nextLine().toLowerCase()) {
                "n" -> false
                "y" -> true
                "" -> true
                else -> throw Exception()
            }
            break
        } catch (e: Exception) {
        }
    }
    return result
}

fun inputFilename(useDefaultFilename: Boolean): String {
    var result: String
    if (!useDefaultFilename) print("Enter file name: ")
    while (true) {
        try {
            result = if (useDefaultFilename) defaultFileName else Scanner(System.`in`).nextLine()
            break
        } catch (e: Exception) {
            print("Enter correct file name (/exit to cancel): ")
        }
    }
    return result
}

fun parseImportant(fileName: String) {
    val path = Paths.get(fileName)
    val bytes = Files.readAllBytes(path)
    parseFileAttrs(path)
    parseSegments(bytes)
}

fun parseFileAttrs(path: Path) {
    val attrs = Files.readAttributes(path, BasicFileAttributes::class.java)
    val formatter = SimpleDateFormat("dd.MM.YYYY - HH:mm:ss")
    addProperty("Creation Time", formatter.formatFileTime(attrs.creationTime()))
    addProperty("Last Access Time", formatter.formatFileTime(attrs.lastAccessTime()))
    addProperty("Last Modified Time", formatter.formatFileTime(attrs.lastModifiedTime()))
    addProperty("Size", "${attrs.size()} bytes")
}

private fun addProperty(name: String, description: String) {
    properties.add("$name = $description")
}

fun parseSegments(bytes: ByteArray): List<String> {
    var index = 0
    val result = mutableListOf<String>()
    while (index < bytes.size) {
        val ff = bytes[index++]
        if (ff != 0xff.toByte())
            return result

        val marker = bytes[index++]
        index = handleMarker(marker, bytes, index)
    }


    return result
}

fun handleMarker(marker: Byte, bytes: ByteArray, startIndex: Int): Int {
    val a = marker.toIntUnsigned()
    val markerType = Marker.values().firstOrNull { it.byte == marker } ?: Marker.UNKNOWN
    return when (marker) {
        0xFE.toByte() -> parseCommentContent(bytes, startIndex)
        0xc0.toByte() -> parseStartOfFrame0(bytes, startIndex)
        0xDA.toByte() -> parseStartOfScanContent(bytes, startIndex)
        0xe0.toByte() -> parseJFIFContent(bytes, startIndex)

        in 0xd0.toByte()..0xd7.toByte() -> parseDefaultContent(bytes, startIndex)
        0xD8.toByte() -> parseDefaultContent(bytes, startIndex)
        0xD9.toByte() -> parseDefaultContent(bytes, startIndex)
        0x01.toByte() -> parseDefaultContent(bytes, startIndex)
        else -> parseDefaultContent(bytes, startIndex, true)
    }
}

fun parseJFIFContent(bytes: ByteArray, startIndex: Int): Int {
    var index = startIndex
    val size = bytesToInt(bytes[index++], bytes[index++])
    val fileIdentifierMark = byteArrayOf(bytes[index++], bytes[index++], bytes[index++], bytes[index++], bytes[index++])
    val majorRevisionNumber = bytes[index++].toIntUnsigned()
    val minorRevisionNumber = bytes[index++].toIntUnsigned()
    val densityUnits = bytes[index++].toIntUnsigned() // 0 - ratio, 1 - dots/inch, 2 - dots/cm
    val xDensity = bytesToInt(bytes[index++], bytes[index++])
    val yDensity = bytesToInt(bytes[index++], bytes[index++])
    val thumbnailWidth = bytes[index++]
    val thumbnailHeight = bytes[index++]

    addProperty("Density x/y", (xDensity.toDouble() / yDensity).toString())
    addProperty("Density Units", when (densityUnits) {
        0 -> "ratio"
        1 -> "dots/inch"
        2 -> "dots/cm"
        else -> "unknown"
    })
    return index
}

fun parseDefaultContent(bytes: ByteArray, startIndex: Int, hasSize: Boolean = false) =
        startIndex + if (hasSize) bytesToInt(bytes[startIndex], bytes[startIndex + 1]) else 0

fun parseCommentContent(bytes: ByteArray, startIndex: Int): Int {
    var index = startIndex
    val size = bytesToInt(bytes[index++], bytes[index++])

    addProperty("Comment", String(createDefaultContent(size, bytes, index)))

    return startIndex + size
}

fun parseStartOfFrame0(bytes: ByteArray, startIndex: Int): Int {
    var index = startIndex
    val size = bytesToInt(bytes[index++], bytes[index++])
    val precision = bytes[index++]
    val height = bytesToInt(bytes[index++], bytes[index++])
    val width = bytesToInt(bytes[index++], bytes[index++])
    val numberOfComponents = bytes[index++]

    for (i in 0 until numberOfComponents) {
        index += 3
    }

    addProperty("Sampling", precision.toString())
    addProperty("Image Width", width.toString())
    addProperty("Image Height", height.toString())
    addProperty("Number Of Components", numberOfComponents.toString())
    addProperty("Color Type", when (numberOfComponents) {
        1.toByte() -> "Gray Scaled"
        3.toByte() -> "YCbCr / YIQ"
        4.toByte() -> "CMYK"
        else -> "unknown"
    })
    return index
}

fun parseStartOfScanContent(bytes: ByteArray, startIndex: Int): Int {
    var index = startIndex

    val headerLength = bytesToInt(bytes[index++], bytes[index++])
    val numberOfComponents = bytes[index++].toIntUnsigned()

    for (i in 0 until numberOfComponents) {
        bytes[index++]
        bytes[index++]
    }
    index += 3

    val imageDataBytes = mutableListOf<Byte>()
    while (true) {
        val byte = bytes[index++]
        if (byte == 0xFF.toByte()) {
            val byte2 = bytes[index++]
            if (byte2 == 0x00.toByte()) {
                imageDataBytes.add(byte)
            } else {
                if (byte2 in 0xd0.toByte()..0xd7.toByte()) {

                } else {
                    index -= 2
                    break
                }
            }
        }
        imageDataBytes.add(byte)
    }

    return index
}

fun createDefaultContent(size: Int, bytes: ByteArray, startIndex: Int) =
        ByteArray(size - 2, { i -> bytes[startIndex + i] })

fun bytesToInt(byte1: Byte, byte2: Byte) = ByteBuffer.wrap(byteArrayOf(0, 0, byte1, byte2)).int
