package main.oop

import main.oop.utils.JpegParser
import main.oop.utils.CatchLooper
import java.util.*


fun main(args: Array<String>) {
    val defaultFileName = "google_favicon.jpg"
    val useDefaultFilename = CatchLooper<Boolean>()
            .onLoop {
                print("Use default picture? (Y̲/N): ")
                when (Scanner(System.`in`).nextLine().toLowerCase()) {
                    "n" -> false
                    "y" -> true
                    "" -> true
                    else -> throw Exception()
                }
            }
            .get()

    val printBaseInfo = CatchLooper<Boolean>()
            .onLoop {
                print("Print only base info? (Y̲/N): ")
                when (Scanner(System.`in`).nextLine().toLowerCase()) {
                    "n" -> false
                    "y" -> true
                    "" -> true
                    else -> throw Exception()
                }
            }
            .get()

    val fileName = CatchLooper<String>()
            .onStart { if (!useDefaultFilename) print("Enter file name: ") }
            .onLoop { if (useDefaultFilename) defaultFileName else Scanner(System.`in`).nextLine() }
            .onError { print("Enter correct file name (/exit to cancel): ") }
            .get()

    if (fileName.toLowerCase() == "/exit") return
    println()
    JpegParser(fileName).apply {
        println(if (printBaseInfo) parseImportant() else parseAll())
    }
}