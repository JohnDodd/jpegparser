package main.oop.structure.content

import main.oop.structure.base.BaseContent
import main.oop.utils.bytesToInt
import main.oop.utils.leftBit
import main.oop.utils.rightBit
import main.oop.utils.toIntUnsigned

class StartOfFrame0(bytes: ByteArray, startIndex: Int) : BaseContent(bytes, startIndex) {

    init {
        val precision = nextByte()
        val height = bytesToInt(nextByte(), nextByte())
        val width = bytesToInt(nextByte(), nextByte())
        val numberOfComponents = nextByte()
        val components = mutableListOf<Component>()
        for (i in 0 until numberOfComponents) {
            components.add(Component(nextByte(), nextByte(), nextByte()))
        }

        val componentSamplingHorizontal = mutableMapOf<Component, Double>()
        val componentSamplingVertical = mutableMapOf<Component, Double>()
        val maxHorizontal = components.maxBy { it.samplingHorizontal }?.samplingHorizontal ?: 0
        val maxVertical = components.maxBy { it.samplingVertical }?.samplingVertical ?: 0

        addProperty("Sampling", precision, true)
        addProperty("Image Width", width, true)
        addProperty("Image Height", height, true)
        addProperty("Number Of Components", numberOfComponents)
        addProperty("Color Type", when (numberOfComponents) {
            1.toByte() -> "Gray Scaled"
            3.toByte() -> "YCbCr / YIQ"
            4.toByte() -> "CMYK"
            else -> "unknown"
        }, true)

        components.forEach {
            componentSamplingHorizontal[it] = maxHorizontal.toDouble() / it.samplingHorizontal
            componentSamplingVertical[it] = maxVertical.toDouble() / it.samplingVertical
            addProperty("Horizontal Sampling by ${it.id} component", componentSamplingHorizontal[it])
            addProperty("Vertical Sampling by ${it.id} component", componentSamplingVertical[it])
        }

    }

    private class Component(byte1: Byte, byte2: Byte, byte3: Byte) {
        val id = byte1.toIntUnsigned()
        val samplingHorizontal = byte2.leftBit()
        val samplingVertical = byte2.rightBit()
        val quantinixationTableID = byte3.toIntUnsigned()

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Component

            if (id != other.id) return false
            if (samplingHorizontal != other.samplingHorizontal) return false
            if (samplingVertical != other.samplingVertical) return false
            if (quantinixationTableID != other.quantinixationTableID) return false

            return true
        }

        override fun hashCode(): Int {
            var result = id
            result = 31 * result + samplingHorizontal
            result = 31 * result + samplingVertical
            result = 31 * result + quantinixationTableID
            return result
        }

    }
}