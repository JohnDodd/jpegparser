package main.oop.structure.content

import main.oop.structure.base.BaseContent

class Comment(bytes: ByteArray, startIndex: Int) : BaseContent(bytes, startIndex) {

    init {
        addProperty("Comment", String(defaultContent), true)
    }
}