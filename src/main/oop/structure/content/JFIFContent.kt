package main.oop.structure.content

import main.oop.structure.base.BaseContent
import main.oop.utils.bytesToInt
import main.oop.utils.toIntUnsigned

class JFIFContent(bytes: ByteArray, startIndex: Int) : BaseContent(bytes, startIndex) {

    init {
        val fileIdentifierMark = byteArrayOf(nextByte(), nextByte(), nextByte(), nextByte(), nextByte())
        val majorRevisionNumber = nextByte().toIntUnsigned()
        val minorRevisionNumber = nextByte().toIntUnsigned()
        val densityUnits = nextByte().toIntUnsigned() // 0 - ratio, 1 - dots/inch, 2 - dots/cm
        val xDensity = bytesToInt(nextByte(), nextByte())
        val yDensity = bytesToInt(nextByte(), nextByte())
        val thumbnailWidth = nextByte()
        val thumbnailHeight = nextByte()

        addProperty("Density x/y", xDensity.toDouble() / yDensity, true)
        addProperty("Density Units", when (densityUnits) {
            0 -> "ratio"
            1 -> "dots/inch"
            2 -> "dots/cm"
            else -> "unknown"
        }, true)
    }

}