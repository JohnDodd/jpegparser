package main.oop.structure.content

import main.oop.structure.base.BaseContent
import main.oop.utils.leftBit
import main.oop.utils.rightBit

class DefineQuantizationTable(bytes: ByteArray, startIndex: Int) : BaseContent(bytes, startIndex) {
    init {
        val info = nextByte()
        val valuesLength = info.leftBit()
        val tableID = info.rightBit()
    }
}