package main.oop.structure.content

import main.oop.structure.base.BaseContent
import main.oop.utils.leftBit
import main.oop.utils.rightBit

class DefineHuffmanTable(bytes: ByteArray, startIndex: Int) : BaseContent(bytes, startIndex) {

    init {
        val info = nextByte()
        val type = info.leftBit() //0 = DC, 1 = AC
        val id = info.rightBit()

        val codeNumbers = ByteArray(16, { i -> nextByte() })
        val codeValues = ByteArray(size - 16 - 3, { i -> nextByte() })


    }
}