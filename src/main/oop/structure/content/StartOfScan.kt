package main.oop.structure.content

import main.oop.structure.base.BaseContent
import main.oop.structure.base.Marker.RST0
import main.oop.structure.base.Marker.RST7
import main.oop.utils.bytesToInt
import main.oop.utils.leftBit
import main.oop.utils.rightBit
import main.oop.utils.toIntUnsigned

class StartOfScan(bytes: ByteArray, startIndex: Int, delegate: (Int) -> Unit) : BaseContent(bytes, startIndex, false) {
    override val size: Int

    init {
        val headerLength = bytesToInt(nextByte(), nextByte())
        val numberOfComponents = nextByte().toIntUnsigned()
        addProperty("Number of components", numberOfComponents)

        val components = mutableListOf<Component>()
        for (i in 0 until numberOfComponents) {
            components.add(Component(nextByte(), nextByte()))
        }
        index += 3
        addProperty("Components", components)

        val imageDataBytes = mutableListOf<Byte>()
        while (true) {
            val byte = nextByte()
            if (byte == 0xFF.toByte()) {
                val byte2 = nextByte()
                if (byte2 == 0x00.toByte()) {
                    imageDataBytes.add(byte)
                } else {
                    if (byte2 in RST0.byte..RST7.byte) {
                        delegate(index - 2)
                    } else {
                        index -= 2
                        break
                    }
                }
            }
            imageDataBytes.add(byte)
        }

        size = index - startIndex
    }

    private class Component(byte1: Byte, byte2: Byte) {

        val id = ID.values().first { it.byte == byte1 }
        val huffmanTableIdDC = byte2.leftBit()
        val huffmanTableIdAC = byte2.rightBit()

        enum class ID(val byte: Byte) {
            Y(1),
            Cb(2),
            Cr(3),
            I(4),
            Q(5)
        }

        override fun toString() = "${id.name} $huffmanTableIdAC$huffmanTableIdDC"
    }
}