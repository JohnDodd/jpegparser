package main.oop.structure.base

import main.oop.utils.bytesToInt
import main.oop.utils.toHex

open class BaseContent(protected val bytes: ByteArray, val startIndex: Int, hasSize: Boolean = true) {
    protected var index = startIndex

    open val size: Int = if (hasSize) bytesToInt(nextByte(), nextByte()) else 0

    override fun toString() = ByteArray(size, { i -> bytes[startIndex + i] }).toHex()

    val isEmpty get() = size == 0

    val isNotEmpty get() = !isEmpty

    val properties = mutableListOf<Property>()

    protected fun addProperty(name: String, description: Any?, isImportant: Boolean = false) =
            properties.add(Property(name, description.toString(), isImportant))

    protected val defaultContent get() = ByteArray(size - 2, { i -> bytes[startIndex + 2 + i] })

    protected fun nextByte() = bytes[index++]

}