package main.oop.structure.base

class Property(val name: String, val description: String, val isImportant: Boolean = false) {
    override fun toString() = "$name = $description"
}