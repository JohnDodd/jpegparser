package main.oop.structure.base

import main.oop.structure.base.Marker.*
import main.oop.structure.content.*
import main.oop.utils.toHexString

open class Segment(private val bytes: ByteArray, private val startIndex: Int, private val delegate: (Int) -> Unit = {}) {

    val marker: Marker
    val offset get() = 2 + content.size

    init {
        val ff = bytes[startIndex]
        val byte = bytes[startIndex + 1]
        marker = Marker.values().firstOrNull { it.byte == byte } ?: UNKNOWN
        if (startIndex == 0 && marker != SOI) throw MarkerException("This is not a jpeg file. Stop parsing.")
        if (ff != 0xFF.toByte()) throw MarkerException("Not a correct segment, index=${startIndex.toHexString()}")
    }

    val content = when (marker) {
        SOS -> StartOfScan(bytes, startIndex + 2, { i -> delegate(i) })
        COM -> Comment(bytes, startIndex + 2)
        SOF0 -> StartOfFrame0(bytes, startIndex + 2)
        DHT -> DefineHuffmanTable(bytes, startIndex + 2)
        DQT -> DefineQuantizationTable(bytes, startIndex + 2)
        APP0 -> JFIFContent(bytes, startIndex + 2)
        else -> BaseContent(bytes, startIndex + 2, marker.hasSize)
    }


    fun createDataArray() = ByteArray(bytes.size - 2).apply { forEachIndexed { i, _ -> bytes[i + 2] } }

}