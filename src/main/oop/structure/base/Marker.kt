package main.oop.structure.base

enum class Marker(val byte: Byte = 0x00, val fullName: String = "Unknown", val hasSize: Boolean = true) {
    COM(0xFE.toByte(), "Comment"),
    DQT(0xDB.toByte(), "Define Quantization Table"),
    SOS(0xDA.toByte(), "Start Of Scan"),
    SOF0(0xc0.toByte(), "Start Of Frame 0"),
    DHT(0xc4.toByte(), "Define Huffman Table"),
    DRI(0xdd.toByte(), "Define Restart Interval"),

    APP0(0xe0.toByte(), "JFIF"),

    APP1(0xe1.toByte(), "Application-specific 1"),
    APP2(0xe2.toByte(), "Application-specific 2"),
    APP3(0xe3.toByte(), "Application-specific 3"),
    APP4(0xe4.toByte(), "Application-specific 4"),
    APP5(0xe5.toByte(), "Application-specific 5"),
    APP6(0xe6.toByte(), "Application-specific 6"),
    APP7(0xe7.toByte(), "Application-specific 7"),
    APP8(0xe8.toByte(), "Application-specific 8"),
    APP9(0xe9.toByte(), "Application-specific 9"),
    APP10(0xea.toByte(), "Application-specific 10"),
    APP11(0xeb.toByte(), "Application-specific 11"),
    APP12(0xec.toByte(), "Application-specific 12"),
    APP13(0xed.toByte(), "Application-specific 13"),
    APP14(0xee.toByte(), "Application-specific 14"),
    APP15(0xef.toByte(), "Application-specific 15"),


    SOI(0xD8.toByte(), "Start Of Image", false),
    EOI(0xD9.toByte(), "End Of Image", false),
    TEM(0x01.toByte(), "TEM", false),
    RST0(0xd0.toByte(), "Restart 0", false),
    RST1(0xd1.toByte(), "Restart 1", false),
    RST2(0xd2.toByte(), "Restart 2", false),
    RST3(0xd3.toByte(), "Restart 3", false),
    RST4(0xd4.toByte(), "Restart 4", false),
    RST5(0xd5.toByte(), "Restart 5", false),
    RST6(0xd6.toByte(), "Restart 6", false),
    RST7(0xd7.toByte(), "Restart 7", false),

    UNKNOWN();

    class MarkerException(message: String) : RuntimeException(message)
}