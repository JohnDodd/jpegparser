package main.oop.utils

import main.oop.structure.base.Marker.MarkerException
import main.oop.structure.base.Property
import main.oop.structure.base.Segment
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.attribute.BasicFileAttributes
import java.text.SimpleDateFormat

class JpegParser(fileName: String) {
    private val path = Paths.get(fileName)
    private val bytes = Files.readAllBytes(path)

    private val fileProperties by lazy {
        val properties = mutableListOf<Property>()
        val attrs = Files.readAttributes(path, BasicFileAttributes::class.java)
        val formatter = SimpleDateFormat("dd.MM.YYYY - HH:mm:ss")

        properties.add(Property("File Name", path.fileName.toString(), true))
        properties.add(Property("Creation Time", formatter.formatFileTime(attrs.creationTime()), true))
        properties.add(Property("Last Access Time", formatter.formatFileTime(attrs.lastAccessTime()), true))
        properties.add(Property("Last Modified Time", formatter.formatFileTime(attrs.lastModifiedTime()), true))
        properties.add(Property("Size", "${attrs.size()} bytes", true))

        properties
    }

    private val segments by lazy {
        Filler<Segment>(
                predicate = { it < bytes.size },
                action = { list, index ->
                    Segment(bytes, index, { list.add(Segment(bytes, it)) })
                            .let { Pair(it.offset, it) }
                },
                onError = {
                    println(it.message)
                    if (it !is MarkerException) throw it
                }
        ).fill()
    }

    fun parseAll() = StringBuilder().apply {
        fileProperties.forEach { if (it.isImportant) append("$it\n") }
        segments.forEach { append(it.toStringFormatted()) }
    }

    fun parseImportant() = StringBuilder().apply {
        fileProperties.forEach { if (it.isImportant) append("$it\n") }
        segments.forEach { it.content.properties.forEach { if (it.isImportant) append("$it\n") } }
    }

    private fun Segment.toStringFormatted(): String {
        val builder = StringBuilder()
        if (content.isNotEmpty) {
            builder.append("\n")
        }
        builder.append("Segment ${marker.fullName}")
        if (content.isNotEmpty) {
            builder.append("[index = ${content.startIndex}, size = ${content.size}] ")
        }
        builder.append(content.properties.toStringFormatted())
        return builder.toString()
    }

}