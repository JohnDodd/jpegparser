package main.oop.utils

class Filler<T>(private val predicate: (Int) -> Boolean, val action: (MutableList<T>, Int) -> Pair<Int, T>, val onError: (Exception) -> Unit) {

    private var index = 0
    private val list = mutableListOf<T>()

    fun fill(): List<T> {
        while (predicate(index)) {
            try {
                val pair = action(list, index)
                index += pair.first
                list.add(pair.second)
            } catch (e: Exception) {
                onError(e)
                break
            }
        }
        return list
    }
}