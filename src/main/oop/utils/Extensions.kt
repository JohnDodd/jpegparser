package main.oop.utils

import main.oop.structure.base.Property
import java.nio.ByteBuffer
import java.nio.file.attribute.FileTime
import java.text.SimpleDateFormat
import java.util.*

val hexArray = "0123456789ABCDEF".toCharArray()

fun ByteArray.toHex(): String {
    val hexChars = CharArray(size * 2)
    for ((index, j) in this.withIndex()) {
        val v = j.normalized()
        hexChars[index * 2] = hexArray[v ushr 4]
        hexChars[index * 2 + 1] = hexArray[v and 0x0F]
    }
    return String(hexChars).apply {
        val buffer = StringBuilder()
        forEachIndexed { index, c ->
            if (index % 2 == 0 && index != 0) buffer.append(" ")
            if (index % 16 == 0 && index != 0) buffer.append("\n")
            buffer.append(c)
        }
        return buffer.toString()
    }
}

fun Byte.normalized() = this.toInt() and 0xFF

fun Byte.toHexString() = this.let {
    val v = normalized()
    "${hexArray[v ushr 4]}${hexArray[v and 0x0F]}"
}


fun Int.toHexString() = "0x" + Integer.toHexString(this).toUpperCase()

fun bytesToInt(byte1: Byte, byte2: Byte) = ByteBuffer.wrap(byteArrayOf(0, 0, byte1, byte2)).int

fun Byte.toIntUnsigned() = bytesToInt(0, this)

fun List<Property>.toStringFormatted() = StringBuilder().also {
    forEach { property -> it.append("\n").append("    ").append(property) }
}

fun Byte.leftBit() = toIntUnsigned() and 0b11110000 shr 4

fun Byte.rightBit() = toIntUnsigned() and 0b00001111

fun SimpleDateFormat.formatFileTime(fileTime: FileTime) = format(Date(fileTime.toMillis()))
