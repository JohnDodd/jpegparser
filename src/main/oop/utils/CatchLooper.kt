package main.oop.utils

class CatchLooper<T>() {
    private var onStart: () -> Unit = {}
    private lateinit var onLoop: () -> T
    private var onError: (e: Exception) -> Unit = {}

    fun onError(onError: (e: Exception) -> Unit): CatchLooper<T> {
        this.onError = onError
        return this
    }

    fun onStart(onStart: () -> Unit): CatchLooper<T> {
        this.onStart = onStart
        return this
    }

    fun onLoop(onLoop: () -> T): CatchLooper<T> {
        this.onLoop = onLoop
        return this
    }

    fun get(): T {
        onStart()
        while (true) {
            try {
                return onLoop()
            } catch (e: Exception) {
                onError(e)
            }
        }
    }
}